test:
	cd NFL/2017-18 && python3 Test_NFL_201718_Monte_Carlo.py && cd ../..
	cd MLB/2016 && python3 Test_MLB_2016_Monte_Carlo.py && cd ../..
	cd MLB/2019 && python3 Test_MLB_2019_Monte_Carlo.py && cd ../..
	cd MLB/2020 && python3 Test_MLB_2020_Monte_Carlo.py && cd ../..
	cd MLB/2021 && python3 Test_MLB_2021_Monte_Carlo.py && cd ../..
	cd Football/MLS/2019/ && python3 Test_MLS_2019_Monte_Carlo.py && cd ../../..
	# cd Football/MLS/2021/ && python3 Test_MLS_2021_Monte_Carlo.py && cd ../../..
	cd Football/PL/2018-19/ && python3 Test_PL_201819_Monte_Carlo.py && cd ../../..
	cd Football/PL/2019-20/ && python3 Test_PL_201920_Monte_Carlo.py && cd ../../..
	cd Football/PL/2020-21/ && python3 Test_PL_202021_Monte_Carlo.py && cd ../../..

format:
	rm -rf venv; rm -rf __pycache__; rm -rf */__pycache__; rm -rf */*/__pycache__; rm -rf */*/*/__pycache__/; black */*/*.py; black */*/*/*.py;
