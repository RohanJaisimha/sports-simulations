def getLeagueWideData(f_name):
    fin = open(f_name, "r")

    runs_for_at_home = []
    runs_for_away = []

    for line in fin:
        line = line.strip().split("\t")
        if len(line) == 2:
            continue
        runs_for_at_home.append(int(line[3]))
        runs_for_away.append(int(line[2]))

    fin.close()

    def mean(a):
        return round(sum(a) / len(a), 5)

    return mean(runs_for_away), mean(runs_for_at_home)


def main():
    f_name = "2020_MLB_Regular_Season_Data_Cleaned.txt"
    getLeagueWideData(f_name)


if __name__ == "__main__":
    main()
