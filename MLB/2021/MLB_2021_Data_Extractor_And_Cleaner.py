from bs4 import BeautifulSoup
import requests
import re


def extractAndClean(url):
    fout = open("MLB_2021_Regular_Season_Data_Cleaned.txt", "w")
    parsed = BeautifulSoup(requests.get(url).text, "html.parser")

    games = parsed.select(".game")

    for game in games:
        if "Spring" in game.text:
            continue

        teams = game.select("a")
        away_team_name = teams[0].text
        home_team_name = teams[1].text

        if away_team_name == "Arizona D'Backs":
            away_team_name = "Arizona Diamondbacks"

        if home_team_name == "Arizona D'Backs":
            home_team_name = "Arizona Diamondbacks"

        if "Boxscore" in game.text:
            away_team_score, home_team_score = re.findall(r"\([\d]+\)", game.text)
            away_team_score = away_team_score[1:-1]
            home_team_score = home_team_score[1:-1]

            fout.write(
                f"{away_team_name}\t{home_team_name}\t{away_team_score}\t{home_team_score}\n"
            )
        else:
            fout.write(f"{away_team_name}\t{home_team_name}\n")

    fout.close()


def main():
    url = "https://www.baseball-reference.com/leagues/MLB/2021-schedule.shtml"
    extractAndClean(url)


if __name__ == "__main__":
    main()
