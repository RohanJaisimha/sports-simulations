from scipy.stats import poisson
import random
import league_wide_averages_getter
import team_specific_averages_getter
from prettytable import PrettyTable

data_file = "MLB_2021_Regular_Season_Data_Cleaned.txt"
table_file = "MLB_2021_Sim_Table.txt"

(
    league_wide_average_runs_for_away,
    league_wide_average_runs_for_at_home,
) = league_wide_averages_getter.getLeagueWideData(data_file)
league_wide_average_runs_against_at_home = league_wide_average_runs_for_away
league_wide_average_runs_against_away = league_wide_average_runs_for_at_home


class Team:
    def __init__(
        self,
        name="",
        average_runs_for_at_home=0,
        average_runs_for_away=0,
        average_runs_against_at_home=0,
        average_runs_against_away=0,
        actual_wins=0,
        simulation_wins=0,
        actual_losses=0,
        simulation_losses=0,
        conference="",
    ):
        self.name = name
        self.average_runs_for_at_home = average_runs_for_at_home
        self.average_runs_for_away = average_runs_for_away
        self.average_runs_against_at_home = average_runs_against_at_home
        self.average_runs_against_away = average_runs_against_away
        self.actual_wins = actual_wins
        self.conference = conference
        self.simulation_wins = simulation_wins
        self.actual_losses = actual_losses
        self.simulation_losses = simulation_losses

        self.attack_strength_home = (
            self.average_runs_for_at_home / league_wide_average_runs_for_at_home
        )
        self.defensive_strength_home = (
            self.average_runs_against_at_home / league_wide_average_runs_against_at_home
        )
        self.attack_strength_away = (
            self.average_runs_for_away / league_wide_average_runs_for_away
        )
        self.defensive_strength_away = (
            self.average_runs_against_away / league_wide_average_runs_against_away
        )
        self.winner = ""

    def __str__(self):
        return self.name


class Match:
    def __init__(self, home_team=Team(), away_team=Team()):
        self.home_team = home_team
        self.away_team = away_team
        self.lambda_home_team = (
            league_wide_average_runs_for_at_home
            * home_team.attack_strength_home
            * away_team.defensive_strength_away
        )
        self.lambda_away_team = (
            league_wide_average_runs_for_away
            * away_team.attack_strength_away
            * home_team.defensive_strength_home
        )
        self.num_runs_home_team = 0
        self.num_runs_away_team = 0

    def sim_match(self, simulation=True, playoff_game=False):
        num_runs_home_team = num_runs_away_team = 0
        while True:
            num_runs_home_team = poisson.ppf(
                random.uniform(0, 1), self.lambda_home_team
            )
            num_runs_away_team = poisson.ppf(
                random.uniform(0, 1), self.lambda_away_team
            )
            if num_runs_away_team != num_runs_home_team:
                break
        self.num_runs_home_team += num_runs_home_team
        self.num_runs_away_team += num_runs_away_team
        self.winner = (
            self.away_team
            if self.num_runs_away_team > self.num_runs_home_team
            else self.home_team
        )
        if not playoff_game:
            self.updateWins(simulation)

    def updateWins(self, simulation=True):
        if simulation:
            if self.num_runs_home_team > self.num_runs_away_team:  # home team wins
                self.home_team.simulation_wins += 1
            elif self.num_runs_home_team < self.num_runs_away_team:  # away team wins
                self.away_team.simulation_wins += 1
        else:
            if self.num_runs_home_team > self.num_runs_away_team:  # home team wins
                self.home_team.actual_wins += 1
            elif self.num_runs_home_team < self.num_runs_away_team:  # away team wins
                self.away_team.actual_wins += 1

    def set_result(self, num_runs_home_team=0, num_runs_away_team=0, simulation=True):
        self.num_runs_home_team += num_runs_home_team
        self.num_runs_away_team += num_runs_away_team
        self.updateWins(simulation)

    def __str__(self):
        return (
            str(self.away_team)
            + " "
            + str(self.num_runs_away_team)
            + " - "
            + str(self.num_runs_home_team)
            + " "
            + str(self.home_team)
        )


class Series:
    def __init__(self, home_team, away_team, num_games):
        self.home_team = home_team
        self.away_team = away_team
        self.num_games = num_games
        self.winner = ""

        if self.num_games == 5:
            self.matches = [
                Match(home_team, away_team),
                Match(home_team, away_team),
                Match(away_team, home_team),
                Match(away_team, home_team),
                Match(home_team, away_team),
            ]
        elif self.num_games == 7:
            self.matches = [
                Match(home_team, away_team),
                Match(home_team, away_team),
                Match(away_team, home_team),
                Match(away_team, home_team),
                Match(away_team, home_team),
                Match(home_team, away_team),
                Match(home_team, away_team),
            ]
        elif self.num_games == 3:
            self.matches = [
                Match(home_team, away_team),
                Match(away_team, home_team),
                Match(home_team, away_team),
            ]
        else:
            raise ValueError("Series.num_games must be 3, 5, or 7")

    def sim_series(self, num_simulations):
        for i in range(self.num_games):
            for j in range(num_simulations):
                self.matches[i].sim_match(playoff_game=True)
            self.matches[i].num_runs_home_team = round(
                self.matches[i].num_runs_home_team / num_simulations, 3
            )
            self.matches[i].num_runs_away_team = round(
                self.matches[i].num_runs_away_team / num_simulations, 3
            )

        num_wins_home_team = 0
        num_wins_away_team = 0
        for i in range(self.num_games):
            if self.matches[i].winner.name == self.home_team.name:
                num_wins_home_team += 1
            else:
                num_wins_away_team += 1
            if num_wins_away_team == self.num_games // 2 + 1:
                self.winner = self.away_team
                break
            if num_wins_home_team == self.num_games // 2 + 1:
                self.winner = self.home_team
                break

    def __str__(self):
        num_wins_home_team = 0
        num_wins_away_team = 0
        index_to_stop_at = 0
        for i in range(self.num_games):
            if self.matches[i].winner.name == self.home_team.name:
                num_wins_home_team += 1
            else:
                num_wins_away_team += 1
            if (
                num_wins_away_team == self.num_games // 2 + 1
                or num_wins_home_team == self.num_games // 2 + 1
            ):
                index_to_stop_at = i + 1
                break

        answer = ""
        for i in range(index_to_stop_at):
            answer = answer + str(self.matches[i]) + "\n"
        return answer


# Creates playoffs and simulates them
# for choice 2 & 3
# Creates playoffs and simulates them
def simPlayoffs(Teams, num_simulations):
    print("Simulating Playoffs...")

    fout = open(table_file, "a")
    NL_East_teams = sorted(
        [i for i in Teams if (i.conference == "NL East")],
        key=lambda a: a.simulation_wins,
        reverse=True,
    )[:3]
    NL_West_teams = sorted(
        [i for i in Teams if (i.conference == "NL West")],
        key=lambda a: a.simulation_wins,
        reverse=True,
    )[:3]
    NL_Central_teams = sorted(
        [i for i in Teams if (i.conference == "NL Central")],
        key=lambda a: a.simulation_wins,
        reverse=True,
    )[:3]
    AL_East_teams = sorted(
        [i for i in Teams if (i.conference == "AL East")],
        key=lambda a: a.simulation_wins,
        reverse=True,
    )[:3]
    AL_West_teams = sorted(
        [i for i in Teams if (i.conference == "AL West")],
        key=lambda a: a.simulation_wins,
        reverse=True,
    )[:3]
    AL_Central_teams = sorted(
        [i for i in Teams if (i.conference == "AL Central")],
        key=lambda a: a.simulation_wins,
        reverse=True,
    )[:3]

    NL_playoff_teams = sorted(
        [NL_East_teams[0], NL_West_teams[0], NL_Central_teams[0]],
        reverse=True,
        key=lambda a: a.simulation_wins,
    )
    NL_wild_card_teams = sorted(
        NL_East_teams[1:3] + NL_West_teams[1:3] + NL_Central_teams[1:3],
        reverse=True,
        key=lambda a: a.simulation_wins,
    )[:2]

    AL_playoff_teams = sorted(
        [AL_East_teams[0], AL_West_teams[0], AL_Central_teams[0]],
        reverse=True,
        key=lambda a: a.simulation_wins,
    )
    AL_wild_card_teams = sorted(
        AL_East_teams[1:3] + AL_West_teams[1:3] + AL_Central_teams[1:3],
        reverse=True,
        key=lambda a: a.simulation_wins,
    )[:2]

    fout.write("\n\nWildcard Games\n^^^^^^^^ ^^^^^\n")
    AL_wild_card_game = Match(*AL_wild_card_teams)
    for i in range(num_simulations):
        AL_wild_card_game.sim_match(playoff_game=True)

    AL_wild_card_game.num_runs_home_team = round(
        AL_wild_card_game.num_runs_home_team / num_simulations, 5
    )
    AL_wild_card_game.num_runs_away_team = round(
        AL_wild_card_game.num_runs_away_team / num_simulations, 5
    )

    fout.write(str(AL_wild_card_game) + "\n\n")

    NL_wild_card_game = Match(*NL_wild_card_teams)
    for i in range(num_simulations):
        NL_wild_card_game.sim_match(playoff_game=True)

    NL_wild_card_game.num_runs_home_team = round(
        NL_wild_card_game.num_runs_home_team / num_simulations, 5
    )
    NL_wild_card_game.num_runs_away_team = round(
        NL_wild_card_game.num_runs_away_team / num_simulations, 5
    )

    fout.write(str(NL_wild_card_game) + "\n")

    fout.write("\nDivision Series\n^^^^^^^^ ^^^^^^\n")
    AL_Division_Series_1 = Series(AL_playoff_teams[0], AL_wild_card_game.winner, 5)
    AL_Division_Series_2 = Series(AL_playoff_teams[1], AL_playoff_teams[2], 5)

    NL_Division_Series_1 = Series(NL_playoff_teams[0], NL_wild_card_game.winner, 5)
    NL_Division_Series_2 = Series(NL_playoff_teams[1], NL_playoff_teams[2], 5)

    AL_Division_Series_1.sim_series(num_simulations)
    AL_Division_Series_2.sim_series(num_simulations)
    NL_Division_Series_1.sim_series(num_simulations)
    NL_Division_Series_2.sim_series(num_simulations)
    fout.write(
        str(AL_Division_Series_1)
        + "\n"
        + str(AL_Division_Series_2)
        + "\n"
        + str(NL_Division_Series_1)
        + "\n"
        + str(NL_Division_Series_2)
    )

    fout.write("\nLeague Championship Series\n^^^^^^ ^^^^^^^^^^^^ ^^^^^^\n")
    AL_Championship_Series_teams = [
        AL_Division_Series_1.winner,
        AL_Division_Series_2.winner,
    ]
    if AL_Championship_Series_teams[0].name == AL_wild_card_game.winner.name:
        AL_Championship_Series_teams = AL_Championship_Series_teams[::-1]
    AL_Championship_Series = Series(*AL_Championship_Series_teams, 7)

    NL_Championship_Series_teams = [
        NL_Division_Series_1.winner,
        NL_Division_Series_2.winner,
    ]
    if NL_Championship_Series_teams[0].name == NL_wild_card_game.winner:
        NL_Championship_Series_teams = NL_Championship_Series_teams[::-1]
    NL_Championship_Series = Series(*NL_Championship_Series_teams, 7)

    AL_Championship_Series.sim_series(num_simulations)
    NL_Championship_Series.sim_series(num_simulations)
    fout.write(str(AL_Championship_Series) + "\n" + str(NL_Championship_Series))

    fout.write("\nWorld Series\n^^^^^^ ^^^^^^\n")
    World_Series_teams = [AL_Championship_Series.winner, NL_Championship_Series.winner]
    World_Series_teams.sort(reverse=True, key=lambda a: a.actual_wins)
    World_Series = Series(*World_Series_teams, 7)
    World_Series.sim_series(num_simulations)
    fout.write(str(World_Series))
    fout.write("\n" + str(World_Series.winner) + " wins the World Series.\n\n")

    fout.close()


def getIndex(Teams, team_name):
    for i in range(len(Teams)):
        if Teams[i].name == team_name:
            return i
    return -1


# Mean Absolute Percentage Error
def MAPE(predicted, actual):
    return round(
        sum([abs((a - p) / a) for a, p in zip(actual, predicted) if a != 0])
        / (len(actual) - actual.count(0))
        * 100,
        5,
    )


# Root Mean Squared Error
def RMSE(predicted, actual):
    return round(
        (sum([(a - p) ** 2 for a, p in zip(actual, predicted)]) / len(actual)) ** 0.5, 5
    )


# simulating all games that have been played so far
# can be used to find the accuracy of this model
# by comparing against the real life table
# choice 1
def simToPointOfLatestData(Teams, num_simulations):
    for i in range(num_simulations):
        fin = open(data_file, "r")
        for line in fin:
            line = line.strip().split("\t")
            home_team = line[1]
            away_team = line[0]
            if len(line) != 2:
                match = Match(
                    Teams[getIndex(Teams, home_team)], Teams[getIndex(Teams, away_team)]
                )
                match.set_result(int(line[3]), int(line[2]), simulation=False)
        fin.close()

    for i in range(num_simulations):
        if num_simulations < 10:
            print("Ran", i, "simulations")
        elif i % (num_simulations // 10) == 0:
            print("Ran", i, "simulations...")
        fin = open(data_file, "r")
        for line in fin:
            line = line.strip().split("\t")
            home_team = line[1]
            away_team = line[0]
            if len(line) != 2:
                match = Match(
                    Teams[getIndex(Teams, home_team)], Teams[getIndex(Teams, away_team)]
                )
                match.sim_match()
        fin.close()

    fout = open(table_file, "w")
    fout.write("Number of simulations: " + str(num_simulations) + "\n\n")
    conferences = sorted(list(set([i.conference for i in Teams])))
    for conference in conferences:
        fout.write("Conference: " + conference + "\n")
        conf_teams = [i for i in Teams if (i.conference == conference)]
        conf_teams.sort(reverse=True, key=lambda a: a.actual_wins)
        output = PrettyTable()
        output.field_names = ["Team Name", "xWins", "Actual wins"]

        for i in conf_teams:
            output.add_row(
                [
                    i.name,
                    i.simulation_wins / num_simulations,
                    i.actual_wins / num_simulations,
                ]
            )

        fout.write(str(output) + "\n\n")

    fout.write(
        "\nMean Absolute Percentage Error: "
        + str(
            MAPE(
                [i.simulation_wins / num_simulations for i in Teams],
                [i.actual_wins / num_simulations for i in Teams],
            )
        )
        + "%\n"
    )
    fout.write(
        "Root Mean Squared Error: "
        + str(
            RMSE(
                [i.simulation_wins / num_simulations for i in Teams],
                [i.actual_wins / num_simulations for i in Teams],
            )
        )
        + " wins\n"
    )

    fout.close()

    print("Finished.")


# real-life results until the the point of latest data, and then
# simulating the remaining games
# choice 2
def simFromPointOfLatestData(Teams, num_simulations):
    for i in range(num_simulations):
        fin = open(data_file, "r")
        for line in fin:
            line = line.strip().split("\t")
            home_team = line[1]
            away_team = line[0]
            if len(line) != 2:
                match = Match(
                    Teams[getIndex(Teams, home_team)], Teams[getIndex(Teams, away_team)]
                )
                match.set_result(int(line[3]), int(line[2]))
        fin.close()

    for i in range(num_simulations):
        if num_simulations < 10:
            print("Ran", i, "simulations")
        elif i % (num_simulations // 10) == 0:
            print("Ran", i, "simulations...")
        fin = open(data_file, "r")
        for line in fin:
            line = line.strip().split("\t")
            home_team = line[1]
            away_team = line[0]
            if len(line) == 2:
                match = Match(
                    Teams[getIndex(Teams, home_team)], Teams[getIndex(Teams, away_team)]
                )
                match.sim_match()
        fin.close()

    fout = open(table_file, "w")
    fout.write("Number of simulations: " + str(num_simulations) + "\n\n")
    conferences = sorted(list(set([i.conference for i in Teams])))
    for conference in conferences:
        fout.write("Conference: " + conference + "\n")
        conf_teams = [i for i in Teams if (i.conference == conference)]
        conf_teams.sort(reverse=True, key=lambda a: a.simulation_wins)
        output = PrettyTable()
        output.field_names = ["Team Name", "xWins"]

        for i in conf_teams:
            output.add_row(
                [
                    i.name,
                    i.simulation_wins / num_simulations,
                ]
            )

        fout.write(str(output) + "\n\n")

    fout.close()
    simPlayoffs(Teams, num_simulations)

    print("Finished.")


# simulating all games
# choice 3
def simFromStart(Teams, num_simulations):
    for i in range(num_simulations):
        if num_simulations < 10:
            print("Ran", i, "simulations")
        elif i % (num_simulations // 10) == 0:
            print("Ran", i, "simulations...")
        fin = open(data_file, "r")
        for line in fin:
            line = line.strip().split("\t")
            home_team = line[1]
            away_team = line[0]
            match = Match(
                Teams[getIndex(Teams, home_team)], Teams[getIndex(Teams, away_team)]
            )
            match.sim_match()
        fin.close()

    fout = open(table_file, "w")
    fout.write("Number of simulations: " + str(num_simulations) + "\n\n")
    conferences = sorted(list(set([i.conference for i in Teams])))
    for conference in conferences:
        fout.write("Conference: " + conference + "\n")
        conf_teams = [i for i in Teams if (i.conference == conference)]
        conf_teams.sort(reverse=True, key=lambda a: a.simulation_wins)
        output = PrettyTable()
        output.field_names = ["Team Name", "xWins"]

        for i in conf_teams:
            output.add_row(
                [
                    i.name,
                    i.simulation_wins / num_simulations,
                ]
            )

        fout.write(str(output) + "\n\n")

    fout.close()
    simPlayoffs(Teams, num_simulations)

    print("Finished.")


# simulate a specific game
# choice 4
def simOneGame(Teams):
    while True:
        print("\n\n\n\n")
        for i in range(len(Teams)):
            print(str(i + 1) + ". " + Teams[i].name)
        away_team_index, home_team_index = input(
            "\n\nEnter the away team's number, a space, then the home team's number: "
        ).split()
        home_team_index = int(home_team_index)
        away_team_index = int(away_team_index)
        num_simulations = int(
            input(
                "Enter the number of simulations you would like to run. For context, 10000 simulations take about five seconds: "
            )
        )
        home_team_runs = away_team_runs = 0
        num_wins_home_team = num_wins_away_team = num_draws = 0
        scorelines = []
        for i in range(num_simulations):
            match = Match(Teams[home_team_index - 1], Teams[away_team_index - 1])
            match.sim_match()
            home_team_runs += match.num_runs_home_team
            away_team_runs += match.num_runs_away_team
            if match.num_runs_home_team > match.num_runs_away_team:
                num_wins_home_team += 1
            elif match.num_runs_home_team < match.num_runs_away_team:
                num_wins_away_team += 1
            scorelines.append(
                str(int(match.num_runs_away_team))
                + " - "
                + str(int(match.num_runs_home_team))
            )

        print("\n\nRan", num_simulations, "simulations.")
        print(
            "Average score was",
            Teams[away_team_index - 1],
            away_team_runs / num_simulations,
            "-",
            home_team_runs / num_simulations,
            Teams[home_team_index - 1],
            ".",
        )

        def mode(l):
            return max(set(l), key=lambda a: l.count(a))

        mode_scoreline = mode(scorelines)
        print(
            "The most common scoreline was " + mode_scoreline + ". It occurred",
            scorelines.count(mode_scoreline),
            "times ("
            + str(round(scorelines.count(mode_scoreline) / num_simulations * 100, 5))
            + "%).",
        )
        print(
            Teams[away_team_index - 1],
            "won",
            num_wins_away_team,
            "games ("
            + str(round(num_wins_away_team / num_simulations * 100, 5))
            + "%).",
        )
        print(
            Teams[home_team_index - 1],
            "won",
            num_wins_home_team,
            "games ("
            + str(round(num_wins_home_team / num_simulations * 100, 5))
            + "%).",
        )
        print("\n\nContinue (Y/N)?")
        choice = input().strip()
        print("You chose", choice)
        if choice.lower() not in ["yes", "y"]:
            break
    print("Finished.")


# displays the menu
# and then directs the program to the required function
def displayMenuThenDirect(Teams):
    print("\nWelcome!\n^^^^^^^")
    print(
        "1. Simulate the season until the point of latest data, in order to check how accurate the model is"
    )
    print(
        "2. Simulate the season using real life results until the point of latest data, then use the model till the end"
    )
    print("3. Simulate all games from the start of the season using the model")
    print("4. Simulate a single game")
    choice = int(input("\nEnter your choice: "))
    if choice == 1:
        num_simulations = int(
            input(
                "Enter how many simulations you would want to run. For context, 100 simulations takes about one minute: "
            )
        )
        simToPointOfLatestData(Teams, num_simulations)
    elif choice == 2:
        num_simulations = int(
            input(
                "Enter how many simulations you would want to run. For context, 100 simulations takes about two minutes: "
            )
        )
        simFromPointOfLatestData(Teams, num_simulations)
    elif choice == 3:
        num_simulations = int(
            input(
                "Enter how many simulations you would want to run. For context, 100 simulations takes about three minutes: "
            )
        )
        simFromStart(Teams, num_simulations)
    elif choice == 4:
        simOneGame(Teams)
    else:
        print("Invalid choice, sorry")


def main():
    Teams = [
        Team(*i) for i in team_specific_averages_getter.getTeamSpecificData(data_file)
    ]
    displayMenuThenDirect(Teams)


if __name__ == "__main__":
    main()
