# Takes in the name of a team and spits
# out what conference that team is in
def getConference(team_name):
    fin = open("MLB_Conferences.txt", "r")
    for line in fin:
        if team_name in line:
            return line[: line.index(":")]


def getTeamSpecificData(f_name):
    teams = set()
    fin = open(f_name, "r")
    teams_by_conference = {}
    for i in fin:
        i = i.split("\t")
        teams.add(i[0])
    fin.close()

    teams = sorted(list(teams))

    for team in teams:
        fin = open(f_name, "r")
        runs_for_at_home = []
        runs_for_away = []
        runs_against_at_home = []
        runs_against_away = []
        for line in fin:
            line = line.strip().split("\t")
            if len(line) == 2:
                continue
            if line[1] == team:
                runs_for_at_home.append(int(line[3]))
                runs_against_at_home.append(int(line[2]))
            elif line[0] == team:
                runs_for_away.append(int(line[2]))
                runs_against_away.append(int(line[3]))

        def mean(a):
            return round(sum(a) / len(a), 5)

        yield (
            team,
            mean(runs_for_at_home),
            mean(runs_for_away),
            mean(runs_against_at_home),
            mean(runs_against_away),
            0,
            0,
            0,
            0,
            getConference(team),
        )
        fin.close()


def main():
    f_name = "MLB_2021_Regular_Season_Data_Cleaned.txt"
    getTeamSpecificData(f_name)


if __name__ == "__main__":
    main()
