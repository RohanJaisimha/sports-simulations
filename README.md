# Sports Simulations
* Programs that simulate MLB games based off real-life results.
* Uses a Poisson distribution to predict number of runs that each team will score in a game, similar to what Dr. Padilla did in [this](https://www.youtube.com/watch?v=Zs2M7gWSbTg) Numberphile video.

# Baseball Simulations
## Files
Each season has six files in it:
* The data file, which will be titled "MLB\_[Year]\_Regular\_Season\_Data\_Cleaned.txt"
* A file that stores the conferences that each team is in, titled "MLB_Conferences.txt"
* "league\_wide\_averages\_getter.py" which is a python program that sifts through the data file and prints out the average goals scored at home, average goals scored away, average goals conceded at home, and average goals conceded away for the league as a whole.
* "team\_specific\_averages\_getter.py" which is a python program that sifts through the data file and prints out the average goals scored at home, average goals scored away, average goals conceded at home, and average goals conceded away for each team.
* The program that runs Monte Carlo simulations, which will be titled "MLB\_[Year]\_Monte\_Carlo.py".
* "Test\_MLB\_[Year]\_Monte\_Carlo.py" which is a python program that contains unit tests for the Monte Carlo simulations
* The program that pulls the data and cleans it, which will be titled "MLB\_[Year]\_Regular\_Season\_Data\_Cleaner.py"

## How to Run
* First, clone/download the repo onto your device. Navigate to the folder. Install the required packages. You may do this using `pip install -r requirements.txt`
* To run the simulations, navigate to the folder for a particular year.
* For seasons that have already reached their end, the data files will already be up to date, so you can just run the Monte Carlo file. You may do this by typing `python3 MLB_[Year]_Monte_Carlo.py`
* For seasons that are in progress, you will first have to update the data file by executing MLB\_[Year]\_Regular\_Season\_Data\_Cleaner.py. This can be done by typing `python3 MLB_[Year]_Regular_Season_Data_Cleaner.py`. Then, you can run the Monte Carlo file. You may do this by typing `python3 MLB_[Year]_Monte_Carlo.py`
* If you chose to simulate only a specific game, then output will be on the console.
* Else, output will be stored into a new file (called "MLB\_[Year]\_Sim\_Table.txt")

# NFL Simulations
## Files
Each season has six files in it:
* The data file, which will be titled "NFL\_[Season]\_Regular\_Season\_Data\_Cleaned.txt"
* A file that stores the conferences that each team is in, titled "NFL_Conferences.txt"
* "league\_wide\_averages\_getter.py" which is a python program that sifts through the data file and prints out the average goals scored at home, average goals scored away, average goals conceded at home, and average goals conceded away for the league as a whole.
* "team\_specific\_averages\_getter.py" which is a python program that sifts through the data file and prints out the average goals scored at home, average goals scored away, average goals conceded at home, and average goals conceded away for each team.
* The program that runs Monte Carlo simulations, which will be titled "NFL\_[Season]\_Monte\_Carlo.py".
* "Test\_NFL\_[Season]\_Monte\_Carlo.py" which is a python program that contains unit tests for the Monte Carlo simulations

## How to Run
* First, clone/download the repo onto your device. Navigate to the folder. Install the required packages. You may do this using `pip install -r requirements.txt`
* To run the simulations, navigate to the folder for a particular season.
* For seasons that have already reached their end, the data files will already be up to date, so you can just run the Monte Carlo file. You may do this by typing `python3 NFL_[Season]_Monte_Carlo.py`
* If you chose to simulate only a specific game, then output will be on the console.
* Else, output will be stored into a new file (called "NFL\_[Season]\_Sim\_Table.txt")

# Football (Soccer) Simulations
## Files
Each season has five files in it:
* The data file, which will be titled "[League]\_[Year/Season]\_Data.txt"
* "league\_wide\_averages\_getter.py" which is a python program that sifts through the data file and returns the average goals scored at home, average goals scored away, average goals conceded at home, and average goals conceded away for the league as a whole.
* "team\_specific\_averages\_getter.py" which is a python program that sifts through the data file and returns the average goals scored at home, average goals scored away, average goals conceded at home, and average goals conceded away for each team.
* The program that runs Monte Carlo simulations,  which will be titled "[League]\_[Year/Season]\_Monte\_Carlo.py".
* "Test\_[League]\_[Year/Season]\_Monte\_Carlo.py" which is a python program that contains unit tests for the Monte Carlo Simulations.
* MLS folders have an extra file in them: "MLS\_Conferences.txt" which is just a folder that contains the name of each conference and the teams in it

## How to Run
* First, clone/download the repo onto your device. Navigate to the folder. Install the required packages. You may do this using `pip install -r requirements.txt`
* Navigate to the folder for a particular season (for Premier League, seasons will be titled like "2018-19"; for MLS, seasons will be titled like "2019").
* For seasons that are in progress, you will have to manually edit the data files so that they are up to date (use tabs not spaces). Premier League directories will have a file that does this for you "PL\_[Season]\_Data\_Extractor\_and\_Cleaner.py," and you can run it using `python3 PL_[Season]_Data_Extractor_and_Cleaner.py`
* To run the simulations, execute [League]\_[Year/Season]\_Monte\_Carlo.py. You can do this by typing ` python3 [League]_[Year/Season]_Monte_Carlo.py`
* If you chose to simulate only a specific game, then output will be on the console.
* Else, output will be stored into a new file (called "[Year]\_[League]\_Sim\_Table.txt")
