import unittest
import sys
import os
import NFL_201718_Monte_Carlo


class Tests(unittest.TestCase):
    def test_monte_carlo_1(self):
        fout = open("input.txt", "w")
        fout.write("1\n2\n")
        fout.close()
        fin = open("input.txt")
        sys.stdin = fin
        NFL_201718_Monte_Carlo.main()
        fin.close()
        os.remove("input.txt")

    def test_monte_carlo_2(self):
        fout = open("input.txt", "w")
        fout.write("2\n30 17\n10\nn")
        fout.close()
        fin = open("input.txt")
        sys.stdin = fin
        NFL_201718_Monte_Carlo.main()
        fin.close()
        os.remove("input.txt")


if __name__ == "__main__":
    unittest.main()
