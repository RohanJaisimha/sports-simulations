from scipy.stats import poisson
import random
import league_wide_averages_getter
import team_specific_averages_getter
from prettytable import PrettyTable

# dataset: https://www.kaggle.com/tobycrabtree/nfl-scores-and-betting-data#spreadspoke_scores.csv
data_file = "NFL_201718_Regular_Season_Data.txt"
table_file = "NFL_201718_Sim_Table.txt"

(
    league_wide_average_points_for_away,
    league_wide_average_points_for_at_home,
) = league_wide_averages_getter.getLeagueWideData(data_file)
league_wide_average_points_against_at_home = league_wide_average_points_for_away
league_wide_average_points_against_away = league_wide_average_points_for_at_home


class Team:
    def __init__(
        self,
        name="",
        average_points_for_at_home=0,
        average_points_for_away=0,
        average_points_against_at_home=0,
        average_points_against_away=0,
        actual_wins=0,
        simulation_wins=0,
        actual_losses=0,
        simulation_losses=0,
        conference="",
    ):
        self.name = name
        self.average_points_for_at_home = average_points_for_at_home
        self.average_points_for_away = average_points_for_away
        self.average_points_against_at_home = average_points_against_at_home
        self.average_points_against_away = average_points_against_away
        self.actual_wins = actual_wins
        self.conference = conference
        self.simulation_wins = simulation_wins
        self.actual_losses = actual_losses
        self.simulation_losses = simulation_losses

        self.attack_strength_home = (
            self.average_points_for_at_home / league_wide_average_points_for_at_home
        )
        self.defensive_strength_home = (
            self.average_points_against_at_home
            / league_wide_average_points_against_at_home
        )
        self.attack_strength_away = (
            self.average_points_for_away / league_wide_average_points_for_away
        )
        self.defensive_strength_away = (
            self.average_points_against_away / league_wide_average_points_against_away
        )

    def __str__(self):
        return self.name


class Match:
    def __init__(self, home_team=Team(), away_team=Team()):
        self.home_team = home_team
        self.away_team = away_team
        self.lambda_home_team = (
            league_wide_average_points_for_at_home
            * home_team.attack_strength_home
            * away_team.defensive_strength_away
        )
        self.lambda_away_team = (
            league_wide_average_points_for_away
            * away_team.attack_strength_away
            * home_team.defensive_strength_home
        )
        self.num_points_home_team = 0
        self.num_points_away_team = 0

    def sim_match(self, simulation=True):
        while True:
            self.num_points_home_team = poisson.ppf(
                random.uniform(0, 1), self.lambda_home_team
            )
            self.num_points_away_team = poisson.ppf(
                random.uniform(0, 1), self.lambda_away_team
            )
            if self.num_points_away_team != self.num_points_home_team:
                break
        self.updatewins(simulation)

    def updatewins(self, simulation=True):
        if simulation:
            if self.num_points_home_team > self.num_points_away_team:  # home team wins
                self.home_team.simulation_wins += 1
            elif (
                self.num_points_home_team < self.num_points_away_team
            ):  # away team wins
                self.away_team.simulation_wins += 1
        else:
            if self.num_points_home_team > self.num_points_away_team:  # home team wins
                self.home_team.actual_wins += 1
            elif (
                self.num_points_home_team < self.num_points_away_team
            ):  # away team wins
                self.away_team.actual_wins += 1

    def set_result(
        self, num_points_home_team=0, num_points_away_team=0, simulation=True
    ):
        self.num_points_home_team = num_points_home_team
        self.num_points_away_team = num_points_away_team
        self.updatewins(simulation)

    def __str__(self):
        return (
            str(self.home_team)
            + " "
            + str(self.num_points_home_team)
            + " - "
            + str(self.num_points_away_team)
            + " "
            + str(self.away_team)
        )


def getIndex(Teams, team_name):
    for i in range(len(Teams)):
        if Teams[i].name == team_name:
            return i
    return -1


# Mean Absolute Percentage Error
def MAPE(predicted, actual):
    return round(
        sum([abs((a - p) / a) for a, p in zip(actual, predicted) if a != 0])
        / (len(actual) - actual.count(0))
        * 100,
        5,
    )


# Root Mean Squared Error
def RMSE(predicted, actual):
    return round(
        (sum([(a - p) ** 2 for a, p in zip(actual, predicted)]) / len(actual)) ** 0.5, 5
    )


# simulating all games to the point of latest data
# can be used to find the accuracy of this model
# by comparing against the real life table
# choice 1
def simToPointOfLatestData(Teams, num_simulations):
    for i in range(num_simulations):
        if num_simulations < 10:
            print("Ran", i, "simulations")
        elif i % (num_simulations // 10) == 0:
            print("Ran", i, "simulations...")
        fin = open(data_file, "r")
        for line in fin:
            line = line.strip().split("\t")
            home_team = line[1]
            away_team = line[0]
            if len(line) != 2:
                match = Match(
                    Teams[getIndex(Teams, home_team)], Teams[getIndex(Teams, away_team)]
                )
                match.sim_match()
                match.set_result(int(line[3]), int(line[2]), simulation=False)
        fin.close()

    fout = open(table_file, "w")
    fout.write("Number of simulations: " + str(num_simulations) + "\n\n\n")
    conferences = sorted(list(set([i.conference for i in Teams])))
    for conference in conferences:
        fout.write("Conference: " + conference + "\n")
        conf_teams = [i for i in Teams if (i.conference == conference)]
        conf_teams.sort(reverse=True, key=lambda a: a.actual_wins)
        output = PrettyTable()
        output.field_names = ["Team Name", "xWins", "Actual wins"]

        for i in conf_teams:
            output.add_row(
                [
                    i.name,
                    i.simulation_wins / num_simulations,
                    i.actual_wins / num_simulations,
                ]
            )

        fout.write(str(output) + "\n\n")

    fout.write(
        "Mean Absolute Percentage Error: "
        + str(
            MAPE(
                [i.simulation_wins / num_simulations for i in Teams],
                [i.actual_wins / num_simulations for i in Teams],
            )
        )
        + "%\n"
    )
    fout.write(
        "Root Mean Squared Error: "
        + str(
            RMSE(
                [i.simulation_wins / num_simulations for i in Teams],
                [i.actual_wins / num_simulations for i in Teams],
            )
        )
        + " wins\n"
    )
    fout.close()
    print("Finished.")


# simulate a specific game
# choice 2
def simOneGame(Teams):
    while True:
        print("\n\n\n\n")
        for i in range(len(Teams)):
            print(str(i + 1) + ". " + Teams[i].name)
        away_team_index, home_team_index = input(
            "\n\nEnter the away team's number, a space, then the home team's number: "
        ).split()
        home_team_index = int(home_team_index)
        away_team_index = int(away_team_index)
        num_simulations = int(
            input(
                "Enter the number of simulations you would like to run. For context, 10000 simulations take about five seconds: "
            )
        )
        home_team_points = away_team_points = 0
        num_wins_home_team = num_wins_away_team = num_draws = 0
        scorelines = []
        for i in range(num_simulations):
            match = Match(Teams[home_team_index - 1], Teams[away_team_index - 1])
            match.sim_match()
            home_team_points += match.num_points_home_team
            away_team_points += match.num_points_away_team
            if match.num_points_home_team > match.num_points_away_team:
                num_wins_home_team += 1
            elif match.num_points_home_team < match.num_points_away_team:
                num_wins_away_team += 1
            scorelines.append(
                str(int(match.num_points_away_team))
                + " - "
                + str(int(match.num_points_home_team))
            )

        print("\n\nRan", num_simulations, "simulations.")
        print(
            "Average score was",
            Teams[away_team_index - 1],
            away_team_points / num_simulations,
            "-",
            home_team_points / num_simulations,
            Teams[home_team_index - 1],
            ".",
        )

        def mode(l):
            return max(set(l), key=lambda a: l.count(a))

        mode_scoreline = mode(scorelines)
        print(
            "The most common scoreline was " + mode_scoreline + ". It occurred",
            scorelines.count(mode_scoreline),
            "times ("
            + str(round(scorelines.count(mode_scoreline) / num_simulations * 100, 5))
            + "%).",
        )
        print(
            Teams[home_team_index - 1],
            "won",
            num_wins_home_team,
            "games ("
            + str(round(num_wins_home_team / num_simulations * 100, 5))
            + "%).",
        )
        print(
            Teams[away_team_index - 1],
            "won",
            num_wins_away_team,
            "games ("
            + str(round(num_wins_away_team / num_simulations * 100, 5))
            + "%).",
        )
        print("\n\nContinue (Y/N)?")
        choice = input().strip()
        print("You chose", choice)
        if choice.lower() not in ["yes", "y"]:
            break
    print("Finished.")


# displays the menu
# and then directs the program to the required function
def displayMenuThenDirect(Teams):
    num_simulations = 10 ** 3
    print("\nWelcome!\n^^^^^^^")
    print("1. Simulate the whole season")
    print("2. Simulate a single game")
    choice = int(input("\nEnter your choice: "))
    if choice == 1:
        num_simulations = int(
            input(
                "Enter how many simulations you would want to run. For context, 1000 simulations takes about a minute: "
            )
        )
        simToPointOfLatestData(Teams, num_simulations)
    elif choice == 2:
        simOneGame(Teams)
    else:
        print("Invalid choice, sorry")


def main():
    Teams = [
        Team(*i) for i in team_specific_averages_getter.getTeamSpecificData(data_file)
    ]
    displayMenuThenDirect(Teams)


if __name__ == "__main__":
    main()
