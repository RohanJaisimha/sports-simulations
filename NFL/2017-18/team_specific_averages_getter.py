# Takes in the name of a team and spits
# out what conference that team is in
def getConference(team_name):
    fin = open("NFL_Conferences.txt", "r")
    for line in fin:
        if team_name in line:
            return line[: line.index(":")]


def getTeamSpecificData(f_name):
    teams = set()
    fin = open(f_name, "r")
    teams_by_conference = {}
    for i in fin:
        i = i.split("\t")
        teams.add(i[0])
    fin.close()

    teams = sorted(list(teams))

    for team in teams:
        fin = open(f_name, "r")
        points_for_at_home = []
        points_for_away = []
        points_against_at_home = []
        points_against_away = []
        for line in fin:
            line = line.strip().split("\t")
            if len(line) == 2:
                continue
            if line[1] == team:
                points_for_at_home.append(int(line[3]))
                points_against_at_home.append(int(line[2]))
            elif line[0] == team:
                points_for_away.append(int(line[2]))
                points_against_away.append(int(line[3]))

        def mean(a):
            return round(sum(a) / len(a), 5)

        yield (
            team,
            mean(points_for_at_home),
            mean(points_for_away),
            mean(points_against_at_home),
            mean(points_against_away),
            0,
            0,
            0,
            0,
            getConference(team),
        )
        fin.close()


def main():
    f_name = "NFL_201718_Regular_Season_Data.txt"
    getTeamSpecificData(f_name)


if __name__ == "__main__":
    main()
