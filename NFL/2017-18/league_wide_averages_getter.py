def getLeagueWideData(f_name):
    fin = open(f_name, "r")

    points_for_at_home = []
    points_for_away = []

    for line in fin:
        line = line.strip().split("\t")
        if len(line) == 2:
            continue
        points_for_at_home.append(int(line[3]))
        points_for_away.append(int(line[2]))

    fin.close()

    def mean(a):
        return round(sum(a) / len(a), 5)

    return mean(points_for_away), mean(points_for_at_home)


def main():
    f_name = "NFL_201718_Regular_Season_Data.txt"
    getLeagueWideData(f_name)


if __name__ == "__main__":
    main()
