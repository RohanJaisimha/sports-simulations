from scipy.stats import poisson
import random
import league_wide_averages_getter
import team_specific_averages_getter
from prettytable import PrettyTable

data_file = "MLS_2021_Regular_Season_Data.txt"
table_file = "MLS_2021_Sim_Table.txt"

(
    league_wide_average_goals_for_away,
    league_wide_average_goals_for_at_home,
) = league_wide_averages_getter.getLeagueWideData(data_file)
league_wide_average_goals_against_at_home = league_wide_average_goals_for_away
league_wide_average_goals_against_away = league_wide_average_goals_for_at_home


class Team:
    def __init__(
        self,
        name="",
        average_goals_for_at_home=0,
        average_goals_for_away=0,
        average_goals_against_at_home=0,
        average_goals_against_away=0,
        actual_points=0,
        simulation_points=0,
        conference="",
        num_games_at_home=0,
        num_games_away=0,
    ):
        self.name = name
        self.average_goals_for_at_home = average_goals_for_at_home
        self.average_goals_for_away = average_goals_for_away
        self.average_goals_against_at_home = average_goals_against_at_home
        self.average_goals_against_away = average_goals_against_away
        self.actual_points = actual_points
        self.conference = conference
        self.simulation_points = simulation_points
        self.num_games_at_home = num_games_at_home
        self.num_games_away = num_games_away
        self.num_points = 0
        self.num_losses = 0

        self.attack_strength_home = (
            self.average_goals_for_at_home / league_wide_average_goals_for_at_home
        )
        self.defensive_strength_home = (
            self.average_goals_against_at_home
            / league_wide_average_goals_against_at_home
        )
        self.attack_strength_away = (
            self.average_goals_for_away / league_wide_average_goals_for_away
        )
        self.defensive_strength_away = (
            self.average_goals_against_away / league_wide_average_goals_against_away
        )

    def __str__(self):
        return self.name


class Match:
    def __init__(self, home_team=Team(), away_team=Team()):
        self.home_team = home_team
        self.away_team = away_team
        self.lambda_home_team = (
            league_wide_average_goals_for_at_home
            * home_team.attack_strength_home
            * away_team.defensive_strength_away
        )
        self.lambda_away_team = (
            league_wide_average_goals_for_away
            * away_team.attack_strength_away
            * home_team.defensive_strength_home
        )
        self.num_goals_home_team = 0
        self.num_goals_away_team = 0
        self.winner = ""

    def sim_match(self, simulation=True, playoff_game=False):
        if not playoff_game:
            self.num_goals_home_team += poisson.ppf(
                random.uniform(0, 1), self.lambda_home_team
            )
            self.num_goals_away_team += poisson.ppf(
                random.uniform(0, 1), self.lambda_away_team
            )
            self.updatePoints(simulation)
        else:
            num_goals_home_team = num_goals_away_team = 0
            while True:
                num_goals_home_team = poisson.ppf(
                    random.uniform(0, 1), self.lambda_home_team
                )
                num_goals_away_team += poisson.ppf(
                    random.uniform(0, 1), self.lambda_away_team
                )
                if num_goals_away_team != num_goals_home_team:
                    break
            self.num_goals_home_team += num_goals_home_team
            self.num_goals_away_team += num_goals_away_team
            if self.num_goals_home_team > self.num_goals_away_team:  # home team points
                self.winner = self.home_team
            elif (
                self.num_goals_home_team < self.num_goals_away_team
            ):  # away team points
                self.winner = self.away_team

    def updatePoints(self, simulation=True):
        if simulation:
            if self.num_goals_home_team > self.num_goals_away_team:  # home team points
                self.home_team.simulation_points += 3
                self.home_team.num_points += 1
                self.winner = self.home_team
            elif (
                self.num_goals_home_team < self.num_goals_away_team
            ):  # away team points
                self.away_team.simulation_points += 3
                self.away_team.num_points += 1
                self.winner = self.away_team
            else:  # draw
                self.home_team.simulation_points += 1
                self.away_team.simulation_points += 1
        else:
            if self.num_goals_home_team > self.num_goals_away_team:  # home team points
                self.home_team.actual_points += 3
                self.home_team.num_points += 1
            elif (
                self.num_goals_home_team < self.num_goals_away_team
            ):  # away team points
                self.away_team.actual_points += 3
                self.away_team.num_points += 1
            else:  # draw
                self.home_team.actual_points += 1
                self.away_team.actual_points += 1

    def set_result(self, num_goals_home_team=0, num_goals_away_team=0, simulation=True):
        self.num_goals_home_team += num_goals_home_team
        self.num_goals_away_team += num_goals_away_team
        self.updatePoints(simulation)

    def __str__(self):
        return (
            str(self.home_team)
            + " "
            + str(self.num_goals_home_team)
            + " - "
            + str(self.num_goals_away_team)
            + " "
            + str(self.away_team)
        )


def getIndex(Teams, team_name):
    for i in range(len(Teams)):
        if Teams[i].name == team_name:
            return i
    return -1


def sortKeySimulation(a):
    return [
        a.simulation_points,
        a.num_points,
        (a.average_goals_for_at_home - a.average_goals_against_at_home)
        * a.num_games_at_home
        + (a.average_goals_for_away - a.average_goals_against_away) * a.num_games_away,
        a.average_goals_for_at_home * a.num_games_at_home
        + a.average_goals_for_away * a.num_games_away,
    ]


def sortKeyReality(a):
    return [
        a.actual_points,
        a.num_points,
        (a.average_goals_for_at_home - a.average_goals_against_at_home)
        * a.num_games_at_home
        + (a.average_goals_for_away - a.average_goals_against_away) * a.num_games_away,
        a.average_goals_for_at_home * a.num_games_at_home
        + a.average_goals_for_away * a.num_games_away,
    ]


# Creates playoffs and simulates them
def simPlayoffs(Teams, num_simulations):
    print("Simulating Playoffs...")

    fout = open(table_file, "a")
    fout.write("\n\nConference Quarter Final Games\n^^^^^^^^^^ ^^^^^^^ ^^^^^ ^^^^^\n")
    eastern_conf_playoff_teams = sorted(
        [i for i in Teams if (i.conference == "Eastern")],
        key=sortKeySimulation,
        reverse=True,
    )[:7]
    western_conf_playoff_teams = sorted(
        [i for i in Teams if (i.conference == "Western")],
        key=sortKeySimulation,
        reverse=True,
    )[:7]
    conference_quarter_final_games = [
        Match(eastern_conf_playoff_teams[1], eastern_conf_playoff_teams[6]),
        Match(eastern_conf_playoff_teams[2], eastern_conf_playoff_teams[5]),
        Match(eastern_conf_playoff_teams[3], eastern_conf_playoff_teams[4]),
        Match(western_conf_playoff_teams[1], western_conf_playoff_teams[6]),
        Match(western_conf_playoff_teams[2], western_conf_playoff_teams[5]),
        Match(western_conf_playoff_teams[3], western_conf_playoff_teams[4]),
    ]

    for i in conference_quarter_final_games:
        if (
            conference_quarter_final_games.index(i)
            == len(conference_quarter_final_games) // 2
        ):
            fout.write("\n")
        for j in range(num_simulations):
            i.sim_match(playoff_game=True)
        fout.write(
            str(i.home_team)
            + " "
            + str(i.num_goals_home_team / num_simulations)
            + " - "
            + str(i.num_goals_away_team / num_simulations)
            + " "
            + str(i.away_team)
            + "\n"
        )
    fout.write("\n\n")

    fout.write("Conference Semi Final Games\n^^^^^^^^^^ ^^^^ ^^^^^ ^^^^^\n")
    conference_semi_final_games = []
    conference_semi_final_games.append(
        Match(eastern_conf_playoff_teams[0], conference_quarter_final_games[2].winner)
    )
    if eastern_conf_playoff_teams.index(
        conference_quarter_final_games[0].winner
    ) < eastern_conf_playoff_teams.index(conference_quarter_final_games[1].winner):
        conference_semi_final_games.append(
            Match(
                conference_quarter_final_games[0].winner,
                conference_quarter_final_games[1].winner,
            )
        )
    else:
        conference_semi_final_games.append(
            Match(
                conference_quarter_final_games[1].winner,
                conference_quarter_final_games[0].winner,
            )
        )

    conference_semi_final_games.append(
        Match(western_conf_playoff_teams[0], conference_quarter_final_games[5].winner)
    )
    if western_conf_playoff_teams.index(
        conference_quarter_final_games[3].winner
    ) < western_conf_playoff_teams.index(conference_quarter_final_games[4].winner):
        conference_semi_final_games.append(
            Match(
                conference_quarter_final_games[3].winner,
                conference_quarter_final_games[4].winner,
            )
        )
    else:
        conference_semi_final_games.append(
            Match(
                conference_quarter_final_games[4].winner,
                conference_quarter_final_games[3].winner,
            )
        )

    for i in conference_semi_final_games:
        if (
            conference_semi_final_games.index(i)
            == len(conference_semi_final_games) // 2
        ):
            fout.write("\n")
        for j in range(num_simulations):
            i.sim_match(playoff_game=True)
        fout.write(
            str(i.home_team)
            + " "
            + str(i.num_goals_home_team / num_simulations)
            + " - "
            + str(i.num_goals_away_team / num_simulations)
            + " "
            + str(i.away_team)
            + "\n"
        )
    fout.write("\n\n")

    fout.write("Conference Final Games\n^^^^^^^^^^ ^^^^^ ^^^^^\n")
    conference_final_games = []
    if eastern_conf_playoff_teams.index(
        conference_semi_final_games[0].winner
    ) < eastern_conf_playoff_teams.index(conference_semi_final_games[1].winner):
        conference_final_games.append(
            Match(
                conference_semi_final_games[0].winner,
                conference_semi_final_games[1].winner,
            )
        )
    else:
        conference_final_games.append(
            Match(
                conference_semi_final_games[1].winner,
                conference_semi_final_games[0].winner,
            )
        )

    if western_conf_playoff_teams.index(
        conference_semi_final_games[3].winner
    ) < western_conf_playoff_teams.index(conference_semi_final_games[2].winner):
        conference_final_games.append(
            Match(
                conference_semi_final_games[3].winner,
                conference_semi_final_games[2].winner,
            )
        )
    else:
        conference_final_games.append(
            Match(
                conference_semi_final_games[2].winner,
                conference_semi_final_games[3].winner,
            )
        )

    for i in conference_final_games:
        if conference_final_games.index(i) == len(conference_final_games) // 2:
            fout.write("\n")
        for j in range(num_simulations):
            i.sim_match(playoff_game=True)
        fout.write(
            str(i.home_team)
            + " "
            + str(i.num_goals_home_team / num_simulations)
            + " - "
            + str(i.num_goals_away_team / num_simulations)
            + " "
            + str(i.away_team)
            + "\n"
        )
    fout.write("\n\n")

    fout.write("MLS Cup Final\n^^^ ^^^ ^^^^^\n")
    MLS_Cup_final_game = ""
    if eastern_conf_playoff_teams.index(
        conference_final_games[0].winner
    ) < western_conf_playoff_teams.index(conference_final_games[1].winner):
        MLS_Cup_final_game = Match(
            conference_final_games[0].winner, conference_final_games[1].winner
        )
    elif eastern_conf_playoff_teams.index(
        conference_final_games[0].winner
    ) > western_conf_playoff_teams.index(conference_final_games[1].winner):
        MLS_Cup_final_game = Match(
            conference_final_games[1].winner, conference_final_games[0].winner
        )
    else:
        home_team = max(
            conference_final_games[1].winner,
            conference_final_games[0].winner,
            key=sortKeySimulation,
        )
        away_team = (
            conference_final_games[1].winner
            if (home_team is conference_final_games[0].winner)
            else conference_final_games[0].winner
        )
        MLS_Cup_final_game = Match(home_team, away_team)

    for i in range(num_simulations):
        MLS_Cup_final_game.sim_match(playoff_game=True)
    fout.write(
        str(MLS_Cup_final_game.home_team)
        + " "
        + str(MLS_Cup_final_game.num_goals_home_team / num_simulations)
        + " - "
        + str(MLS_Cup_final_game.num_goals_away_team / num_simulations)
        + " "
        + str(MLS_Cup_final_game.away_team)
        + "\n"
    )
    fout.close()


# Mean Absolute Percentage Error
def MAPE(predicted, actual):
    return round(
        sum([abs((a - p) / a) for a, p in zip(actual, predicted) if a != 0])
        / (len(actual) - actual.count(0))
        * 100,
        5,
    )


# Root Mean Squared Error
def RMSE(predicted, actual):
    return round(
        (sum([(a - p) ** 2 for a, p in zip(actual, predicted)]) / len(actual)) ** 0.5, 5
    )


# simulating all games that have been played so far
# can be used to find the accuracy of this model
# by comparing against the real life table
# choice 1
def simToPointOfLatestData(Teams, num_simulations):
    for i in range(num_simulations):
        fin = open(data_file, "r")
        for line in fin:
            line = line.strip().split("\t")
            home_team = line[1]
            away_team = line[0]
            if len(line) != 2:
                match = Match(
                    Teams[getIndex(Teams, home_team)], Teams[getIndex(Teams, away_team)]
                )
                match.set_result(int(line[3]), int(line[2]), simulation=False)
        fin.close()

    for i in range(num_simulations):
        if num_simulations < 10:
            print("Ran", i, "simulations")
        elif i % (num_simulations // 10) == 0:
            print("Ran", i, "simulations...")
        fin = open(data_file, "r")
        for line in fin:
            line = line.strip().split("\t")
            home_team = line[1]
            away_team = line[0]
            if len(line) != 2:
                match = Match(
                    Teams[getIndex(Teams, home_team)], Teams[getIndex(Teams, away_team)]
                )
                match.sim_match()
        fin.close()

    fout = open(table_file, "w")
    fout.write("Number of simulations: " + str(num_simulations) + "\n\n")
    conferences = sorted(list(set([i.conference for i in Teams])))
    for conference in conferences:
        fout.write("Conference: " + conference + "\n")
        conf_teams = [i for i in Teams if (i.conference == conference)]
        conf_teams.sort(reverse=True, key=lambda a: a.actual_points)
        output = PrettyTable()
        output.field_names = ["Team Name", "xPoints", "Actual points"]

        for i in conf_teams:
            output.add_row(
                [
                    i.name,
                    i.simulation_points / num_simulations,
                    i.actual_points / num_simulations,
                ]
            )

        fout.write(str(output) + "\n\n")

    fout.write(
        "\nMean Absolute Percentage Error: "
        + str(
            MAPE(
                [i.simulation_points / num_simulations for i in Teams],
                [i.actual_points / num_simulations for i in Teams],
            )
        )
        + "%\n"
    )
    fout.write(
        "Root Mean Squared Error: "
        + str(
            RMSE(
                [i.simulation_points / num_simulations for i in Teams],
                [i.actual_points / num_simulations for i in Teams],
            )
        )
        + " points\n"
    )

    fout.close()

    print("Finished.")


# real-life results until the the point of latest data, and then
# simulating the remaining games
# choice 2
def simFromPointOfLatestData(Teams, num_simulations):
    for i in range(num_simulations):
        fin = open(data_file, "r")
        for line in fin:
            line = line.strip().split("\t")
            home_team = line[1]
            away_team = line[0]
            if len(line) != 2:
                match = Match(
                    Teams[getIndex(Teams, home_team)], Teams[getIndex(Teams, away_team)]
                )
                match.set_result(int(line[3]), int(line[2]))
        fin.close()

    for i in range(num_simulations):
        if num_simulations < 10:
            print("Ran", i, "simulations")
        elif i % (num_simulations // 10) == 0:
            print("Ran", i, "simulations...")
        fin = open(data_file, "r")
        for line in fin:
            line = line.strip().split("\t")
            home_team = line[1]
            away_team = line[0]
            if len(line) == 2:
                match = Match(
                    Teams[getIndex(Teams, home_team)], Teams[getIndex(Teams, away_team)]
                )
                match.sim_match()
        fin.close()

    fout = open(table_file, "w")
    fout.write("Number of simulations: " + str(num_simulations) + "\n\n")
    conferences = sorted(list(set([i.conference for i in Teams])))
    for conference in conferences:
        fout.write("Conference: " + conference + "\n")
        conf_teams = [i for i in Teams if (i.conference == conference)]
        conf_teams.sort(reverse=True, key=lambda a: a.simulation_points)
        output = PrettyTable()
        output.field_names = ["Team Name", "xPoints"]

        for i in conf_teams:
            output.add_row(
                [
                    i.name,
                    i.simulation_points / num_simulations,
                ]
            )

        fout.write(str(output) + "\n\n")

    fout.close()
    simPlayoffs(Teams, num_simulations)

    print("Finished.")


# simulating all games
# choice 3
def simFromStart(Teams, num_simulations):
    for i in range(num_simulations):
        if num_simulations < 10:
            print("Ran", i, "simulations")
        elif i % (num_simulations // 10) == 0:
            print("Ran", i, "simulations...")
        fin = open(data_file, "r")
        for line in fin:
            line = line.strip().split("\t")
            home_team = line[1]
            away_team = line[0]
            match = Match(
                Teams[getIndex(Teams, home_team)], Teams[getIndex(Teams, away_team)]
            )
            match.sim_match()
        fin.close()

    fout = open(table_file, "w")
    fout.write("Number of simulations: " + str(num_simulations) + "\n\n")
    conferences = sorted(list(set([i.conference for i in Teams])))
    for conference in conferences:
        fout.write("Conference: " + conference + "\n")
        conf_teams = [i for i in Teams if (i.conference == conference)]
        conf_teams.sort(reverse=True, key=lambda a: a.simulation_points)
        output = PrettyTable()
        output.field_names = ["Team Name", "xPoints"]

        for i in conf_teams:
            output.add_row(
                [
                    i.name,
                    i.simulation_points / num_simulations,
                ]
            )

        fout.write(str(output) + "\n\n")

    fout.close()
    simPlayoffs(Teams, num_simulations)

    print("Finished.")


# simulate a specific game
# choice 4
def simOneGame(Teams):
    while True:
        print("\n\n\n\n")
        for i in range(len(Teams)):
            print(str(i + 1) + ". " + Teams[i].name)
        away_team_index, home_team_index = input(
            "\n\nEnter the away team's number, a space, then the home team's number: "
        ).split()
        home_team_index = int(home_team_index)
        away_team_index = int(away_team_index)
        num_simulations = int(
            input(
                "Enter the number of simulations you would like to run. For context, 10000 simulations take about five seconds: "
            )
        )
        home_team_runs = away_team_runs = 0
        num_points_home_team = num_points_away_team = num_draws = 0
        scorelines = []
        for i in range(num_simulations):
            match = Match(Teams[home_team_index - 1], Teams[away_team_index - 1])
            match.sim_match()
            home_team_runs += match.num_runs_home_team
            away_team_runs += match.num_runs_away_team
            if match.num_runs_home_team > match.num_runs_away_team:
                num_points_home_team += 1
            elif match.num_runs_home_team < match.num_runs_away_team:
                num_points_away_team += 1
            scorelines.append(
                str(int(match.num_runs_away_team))
                + " - "
                + str(int(match.num_runs_home_team))
            )

        print("\n\nRan", num_simulations, "simulations.")
        print(
            "Average score was",
            Teams[away_team_index - 1],
            away_team_runs / num_simulations,
            "-",
            home_team_runs / num_simulations,
            Teams[home_team_index - 1],
            ".",
        )

        def mode(l):
            return max(set(l), key=lambda a: l.count(a))

        mode_scoreline = mode(scorelines)
        print(
            "The most common scoreline was " + mode_scoreline + ". It occurred",
            scorelines.count(mode_scoreline),
            "times ("
            + str(round(scorelines.count(mode_scoreline) / num_simulations * 100, 5))
            + "%).",
        )
        print(
            Teams[away_team_index - 1],
            "won",
            num_points_away_team,
            "games ("
            + str(round(num_points_away_team / num_simulations * 100, 5))
            + "%).",
        )
        print(
            Teams[home_team_index - 1],
            "won",
            num_points_home_team,
            "games ("
            + str(round(num_points_home_team / num_simulations * 100, 5))
            + "%).",
        )
        print("\n\nContinue (Y/N)?")
        choice = input().strip()
        print("You chose", choice)
        if choice.lower() not in ["yes", "y"]:
            break
    print("Finished.")


# displays the menu
# and then directs the program to the required function
def displayMenuThenDirect(Teams):
    print("\nWelcome!\n^^^^^^^")
    print(
        "1. Simulate the season until the point of latest data, in order to check how accurate the model is"
    )
    print(
        "2. Simulate the season using real life results until the point of latest data, then use the model till the end"
    )
    print("3. Simulate all games from the start of the season using the model")
    print("4. Simulate a single game")
    choice = int(input("\nEnter your choice: "))
    if choice == 1:
        num_simulations = int(
            input(
                "Enter how many simulations you would want to run. For context, 1000 simulations takes about one minute: "
            )
        )
        simToPointOfLatestData(Teams, num_simulations)
    elif choice == 2:
        num_simulations = int(
            input(
                "Enter how many simulations you would want to run. For context, 1000 simulations takes about two minutes: "
            )
        )
        simFromPointOfLatestData(Teams, num_simulations)
    elif choice == 3:
        num_simulations = int(
            input(
                "Enter how many simulations you would want to run. For context, 1000 simulations takes about three minutes: "
            )
        )
        simFromStart(Teams, num_simulations)
    elif choice == 4:
        simOneGame(Teams)
    else:
        print("Invalid choice, sorry")


def main():
    Teams = [
        Team(*i) for i in team_specific_averages_getter.getTeamSpecificData(data_file)
    ]
    displayMenuThenDirect(Teams)


if __name__ == "__main__":
    main()
