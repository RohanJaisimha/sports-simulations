# Takes in the name of a team and spit out the conference
# that the team is in


def getConference(team_name):
    fin = open("MLS_Conferences_2021.txt", "r")
    for line in fin:
        if team_name in line:
            return line[: line.index(":")]


def getTeamSpecificData(f_name):
    teams = set()
    fin = open(f_name, "r")
    for i in fin:
        i = i.split("\t")
        teams.add(i[0])
    fin.close()

    teams = sorted(list(teams))
    for team in teams:
        fin = open(f_name, "r")
        goals_for_at_home = []
        goals_for_away = []
        goals_against_at_home = []
        goals_against_away = []
        for line in fin:
            line = line.strip().split("\t")
            if len(line) == 2:
                continue
            if line[0] == team:
                goals_for_at_home.append(int(line[2]))
                goals_against_at_home.append(int(line[3]))
            elif line[1] == team:
                goals_for_away.append(int(line[3]))
                goals_against_away.append(int(line[2]))

        def mean(a):
            try:
                return round(sum(a) / len(a), 5)
            except ZeroDivisionError:
                return 1.35

        yield (
            team,
            mean(goals_for_at_home),
            mean(goals_for_away),
            mean(goals_against_at_home),
            mean(goals_against_away),
            0,
            0,
            getConference(team),
            len(goals_for_at_home),
            len(goals_for_away),
        )
        fin.close()


def main():
    f_name = "2021_MLS_Regular_Season_Data.txt"
    getTeamSpecificData(f_name)


if __name__ == "__main__":
    main()
