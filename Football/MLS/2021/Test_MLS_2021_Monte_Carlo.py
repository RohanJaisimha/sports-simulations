import unittest
import sys
import os
import MLS_2021_Monte_Carlo


class Tests(unittest.TestCase):
    def test_monte_carlo_1(self):
        fout = open("input.txt", "w")
        fout.write("1\n2\n")
        fout.close()
        fin = open("input.txt")
        sys.stdin = fin
        MLS_2021_Monte_Carlo.main()
        fout.close()
        fin.close()
        os.remove("input.txt")

    def test_monte_carlo_2(self):
        fout = open("input.txt", "w")
        fout.write("2\n2\n")
        fout.close()
        fin = open("input.txt")
        sys.stdin = fin
        MLS_2021_Monte_Carlo.main()
        fin.close()
        os.remove("input.txt")

    def test_monte_carlo_3(self):
        fout = open("input.txt", "w")
        fout.write("3\n2\n")
        fout.close()
        fin = open("input.txt")
        sys.stdin = fin
        MLS_2021_Monte_Carlo.main()
        fin.close()
        os.remove("input.txt")

    def test_monte_carlo_4(self):
        fout = open("input.txt", "w")
        fout.write("4\n10 11\n10\nn")
        fout.close()
        fin = open("input.txt")
        sys.stdin = fin
        MLS_2021_Monte_Carlo.main()
        fin.close()
        os.remove("input.txt")


if __name__ == "__main__":
    unittest.main()
