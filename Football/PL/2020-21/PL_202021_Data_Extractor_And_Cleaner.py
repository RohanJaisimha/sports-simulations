from bs4 import BeautifulSoup, Comment
import requests
import sys


def extractAndClean(url):
    fout = open("PL_202021_Data.txt", "w")
    parsed = BeautifulSoup(requests.get(url).text, "html.parser")

    data = []
    tds = parsed.findAll("td")
    for td in tds:
        if td["data-stat"] not in ["squad_a", "xg_a", "score", "xg_b", "squad_b"]:
            continue
        text = td.text.strip()
        if not text:
            continue
        if td["data-stat"] == "score":
            data.extend(text.split(chr(8211)))
        else:
            data.append(text)

    isNumeric = lambda a: a.replace(".", "", 1).isdigit()

    i = 0
    while i < len(data):
        if isNumeric(data[i + 1]):
            if isNumeric(data[i + 3]):
                fout.write(
                    data[i]
                    + "\t"
                    + data[i + 5]
                    + "\t"
                    + data[i + 2]
                    + "\t"
                    + data[i + 3]
                    + "\t"
                    + data[i + 1]
                    + "\t"
                    + data[i + 4]
                    + "\n"
                )
                i += 6
            else:
                fout.write(
                    data[i]
                    + "\t"
                    + data[i + 3]
                    + "\t"
                    + data[i + 1]
                    + "\t"
                    + data[i + 2]
                    + "\n"
                )
                i += 4
        else:
            fout.write(data[i] + "\t" + data[i + 1] + "\n")
            i += 2

    fout.close()


def main():
    url = "https://fbref.com/en/comps/9/schedule/Premier-League-Scores-and-Fixtures"
    extractAndClean(url)


if __name__ == "__main__":
    main()
