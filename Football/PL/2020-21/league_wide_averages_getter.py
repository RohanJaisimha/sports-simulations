def getLeagueWideData(f_name):
    fin = open(f_name, "r")

    xG_for_at_home = []
    xG_for_away = []

    for line in fin:
        line = line.strip().split("\t")
        if len(line) == 2:
            continue
        xG_for_at_home.append(float(line[4]))
        xG_for_away.append(float(line[5]))

    fin.close()

    def mean(a):
        return round(sum(a) / len(a), 5)

    return mean(xG_for_away), mean(xG_for_at_home)


def main():
    f_name = "PL_202021_Data.txt"
    getLeagueWideData(f_name)


if __name__ == "__main__":
    main()
