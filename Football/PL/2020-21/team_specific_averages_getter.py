def getTeamSpecificData(f_name):
    teams = set()
    fin = open(f_name, "r")
    for i in fin:
        i = i.split("\t")
        teams.add(i[0])
    fin.close()

    teams = sorted(list(teams))
    for team in teams:
        fin = open(f_name, "r")
        xG_for_at_home = []
        xG_for_away = []
        xG_against_at_home = []
        xG_against_away = []
        for line in fin:
            line = line.strip().split("\t")
            if len(line) == 2:
                continue
            if line[0] == team:
                xG_for_at_home.append(float(line[4]))
                xG_against_at_home.append(float(line[5]))
            elif line[1] == team:
                xG_for_away.append(float(line[5]))
                xG_against_away.append(float(line[4]))

        def mean(a):
            return round(sum(a) / len(a), 5)

        yield (
            team,
            mean(xG_for_at_home),
            mean(xG_for_away),
            mean(xG_against_at_home),
            mean(xG_against_away),
            0,
            0,
            len(xG_for_at_home),
            len(xG_for_away),
        )
        fin.close()


def main():
    f_name = "PL_202021_Data.txt"
    getTeamSpecificData(f_name)


if __name__ == "__main__":
    main()
