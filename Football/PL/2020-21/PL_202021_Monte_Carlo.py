from scipy.stats import poisson
import random
import league_wide_averages_getter
import team_specific_averages_getter
from prettytable import PrettyTable

data_file = "PL_202021_Data.txt"
table_file = "PL_202021_Sim_Table.txt"

(
    league_wide_average_goals_for_away,
    league_wide_average_goals_for_at_home,
) = league_wide_averages_getter.getLeagueWideData(data_file)
league_wide_average_goals_against_at_home = league_wide_average_goals_for_away
league_wide_average_goals_against_away = league_wide_average_goals_for_at_home


class Team:
    def __init__(
        self,
        name="",
        average_goals_for_at_home=0,
        average_goals_for_away=0,
        average_goals_against_at_home=0,
        average_goals_against_away=0,
        actual_points=0,
        simulation_points=0,
        num_games_at_home=0,
        num_games_away=0,
    ):
        self.name = name
        self.average_goals_for_at_home = average_goals_for_at_home
        self.average_goals_for_away = average_goals_for_away
        self.average_goals_against_at_home = average_goals_against_at_home
        self.average_goals_against_away = average_goals_against_away
        self.actual_points = actual_points
        self.simulation_points = simulation_points
        self.num_games_at_home = num_games_at_home
        self.num_games_away = num_games_away

        self.attack_strength_home = (
            self.average_goals_for_at_home / league_wide_average_goals_for_at_home
        )
        self.defensive_strength_home = (
            self.average_goals_against_at_home
            / league_wide_average_goals_against_at_home
        )
        self.attack_strength_away = (
            self.average_goals_for_away / league_wide_average_goals_for_away
        )
        self.defensive_strength_away = (
            self.average_goals_against_away / league_wide_average_goals_against_away
        )

    def __str__(self):
        return self.name


class Match:
    def __init__(self, home_team=Team(), away_team=Team()):
        self.home_team = home_team
        self.away_team = away_team
        self.lambda_home_team = (
            league_wide_average_goals_for_at_home
            * home_team.attack_strength_home
            * away_team.defensive_strength_away
        )
        self.lambda_away_team = (
            league_wide_average_goals_for_away
            * away_team.attack_strength_away
            * home_team.defensive_strength_home
        )
        self.num_goals_home_team = 0
        self.num_goals_away_team = 0

    def sim_match(self, simulation=True):
        self.num_goals_home_team += poisson.ppf(
            random.uniform(0, 1), self.lambda_home_team
        )
        self.num_goals_away_team += poisson.ppf(
            random.uniform(0, 1), self.lambda_away_team
        )
        self.updatePoints(simulation)

    def updatePoints(self, simulation=True):
        if simulation:
            if self.num_goals_home_team > self.num_goals_away_team:  # home team wins
                self.home_team.simulation_points += 3
            elif self.num_goals_home_team < self.num_goals_away_team:  # away team wins
                self.away_team.simulation_points += 3
            else:  # draw
                self.home_team.simulation_points += 1
                self.away_team.simulation_points += 1
        else:
            if self.num_goals_home_team > self.num_goals_away_team:  # home team wins
                self.home_team.actual_points += 3
            elif self.num_goals_home_team < self.num_goals_away_team:  # away team wins
                self.away_team.actual_points += 3
            else:  # draw
                self.home_team.actual_points += 1
                self.away_team.actual_points += 1

    def set_result(self, num_goals_home_team=0, num_goals_away_team=0, simulation=True):
        self.num_goals_home_team += num_goals_home_team
        self.num_goals_away_team += num_goals_away_team
        self.updatePoints(simulation)

    def __str__(self):
        return (
            str(self.home_team)
            + " "
            + str(self.num_goals_home_team)
            + " - "
            + str(self.num_goals_away_team)
            + " "
            + str(self.away_team)
        )


def getIndex(Teams, team_name):
    for i in range(len(Teams)):
        if Teams[i].name == team_name:
            return i
    return -1


def sortKeySimulation(a):
    return [
        a.simulation_points,
        (a.average_goals_for_at_home - a.average_goals_against_at_home)
        * a.num_games_at_home
        + (a.average_goals_for_away - a.average_goals_against_away) * a.num_games_away,
        a.average_goals_for_at_home * a.num_games_at_home
        + a.average_goals_for_away * a.num_games_away,
    ]


def sortKeyReality(a):
    return [
        a.actual_points,
        (a.average_goals_for_at_home - a.average_goals_against_at_home)
        * a.num_games_at_home
        + (a.average_goals_for_away - a.average_goals_against_away) * a.num_games_away,
        a.average_goals_for_at_home * a.num_games_at_home
        + a.average_goals_for_away * a.num_games_away,
    ]


# Mean Absolute Percentage Error
def MAPE(predicted, actual):
    return round(
        sum([abs((a - p) / a) for a, p in zip(actual, predicted) if a != 0])
        / (len(actual) - actual.count(0))
        * 100,
        5,
    )


# Root Mean Squared Error
def RMSE(predicted, actual):
    return round(
        (sum([(a - p) ** 2 for a, p in zip(actual, predicted)]) / len(actual)) ** 0.5, 5
    )


# simulate the whole season
# choice 1
def simWholeSeason(Teams, num_simulations):
    for i in range(num_simulations):
        fin = open(data_file, "r")
        for line in fin:
            line = line.strip().split("\t")
            home_team = line[0]
            away_team = line[1]
            match = Match(
                Teams[getIndex(Teams, home_team)], Teams[getIndex(Teams, away_team)]
            )
            match.set_result(int(line[2]), int(line[3]), simulation=False)
        fin.close()

    for i in range(num_simulations):
        fin = open(data_file, "r")
        if num_simulations < 10:
            print("Ran", i, "simulations...")
        elif i % (num_simulations // 10) == 0:
            print("Ran", i, "simulations...")
        for line in fin:
            line = line.strip().split("\t")
            home_team = line[0]
            away_team = line[1]
            match = Match(
                Teams[getIndex(Teams, home_team)], Teams[getIndex(Teams, away_team)]
            )
            match.sim_match()
        fin.close()

    fout = open(table_file, "w")
    Teams.sort(reverse=True, key=lambda a: a.actual_points)
    fout.write("Number of simulations: " + str(num_simulations) + "\n\n\n")
    output = PrettyTable()
    output.field_names = ["Team Name", "xPoints", "Actual Points"]

    for i in Teams:
        output.add_row(
            [
                i.name,
                i.simulation_points / num_simulations,
                i.actual_points / num_simulations,
            ]
        )

    fout.write(str(output) + "\n")
    fout.write(
        "\n\nMean Absolute Percentage Error: "
        + str(
            MAPE(
                [i.simulation_points / num_simulations for i in Teams],
                [i.actual_points / num_simulations for i in Teams],
            )
        )
        + "%\n"
    )
    fout.write(
        "Root Mean Squared Error: "
        + str(
            RMSE(
                [i.simulation_points / num_simulations for i in Teams],
                [i.actual_points / num_simulations for i in Teams],
            )
        )
        + " points\n"
    )

    fout.close()
    print("Finished.")


# simulate a specific game
# choice 2
def simOneGame(Teams):
    while True:
        print("\n\n\n\n")
        for i in range(len(Teams)):
            print(str(i + 1) + ". " + Teams[i].name)
        home_team_index, away_team_index = input(
            "\n\nEnter the home team's number, a space, then the away team's number: "
        ).split()
        home_team_index = int(home_team_index)
        away_team_index = int(away_team_index)
        num_simulations = int(
            input(
                "Enter the number of simulations you would like to run. For context, 10000 simulations take about five seconds: "
            )
        )
        home_team_goals = away_team_goals = 0
        num_wins_home_team = num_wins_away_team = num_draws = 0
        scorelines = []
        for i in range(num_simulations):
            match = Match(Teams[home_team_index - 1], Teams[away_team_index - 1])
            match.sim_match()
            home_team_goals += match.num_goals_home_team
            away_team_goals += match.num_goals_away_team
            if match.num_goals_home_team > match.num_goals_away_team:
                num_wins_home_team += 1
            elif match.num_goals_home_team < match.num_goals_away_team:
                num_wins_away_team += 1
            else:
                num_draws += 1
            scorelines.append(
                str(int(match.num_goals_home_team))
                + " - "
                + str(int(match.num_goals_away_team))
            )

        print("\n\nRan", num_simulations, "simulations.")
        print(
            "Average score was",
            Teams[home_team_index - 1],
            home_team_goals / num_simulations,
            "-",
            away_team_goals / num_simulations,
            Teams[away_team_index - 1],
            ".",
        )

        def mode(l):
            return max(set(l), key=lambda a: l.count(a))

        mode_scoreline = mode(scorelines)
        print(
            "The most common scoreline was " + mode_scoreline + ". It occurred",
            scorelines.count(mode_scoreline),
            "times ("
            + str(round(scorelines.count(mode_scoreline) / num_simulations * 100, 5))
            + "%).",
        )
        print(
            Teams[home_team_index - 1],
            "won",
            num_wins_home_team,
            "games ("
            + str(round(num_wins_home_team / num_simulations * 100, 5))
            + "%).",
        )
        print(
            Teams[away_team_index - 1],
            "won",
            num_wins_away_team,
            "games ("
            + str(round(num_wins_away_team / num_simulations * 100, 5))
            + "%).",
        )
        print(
            num_draws,
            "games were drawn ("
            + str(round(num_draws / num_simulations * 100, 5))
            + "%).",
        )
        print("\n\nContinue (Y/N)?")
        choice = input().strip()
        print("You chose", choice)
        if choice.lower() not in ["yes", "y"]:
            break
    print("Finished.")


# displays the menu
# and then directs the program to the required function
def displayMenuThenDirect(Teams):
    print("\nWelcome!\n^^^^^^^^")
    print("1. Sim whole season")
    print("2. Sim one game")
    choice = int(input("\nEnter your choice: "))
    if choice == 1:
        num_simulations = int(
            input(
                "Enter the number of simulations you would like to run. For context, 1000 simulations take about three minutes: "
            )
        )
        simWholeSeason(Teams, num_simulations)
    elif choice == 2:
        simOneGame(Teams)
    else:
        print("Invalid choice, sorry")


def main():
    Teams = [
        Team(*i) for i in team_specific_averages_getter.getTeamSpecificData(data_file)
    ]
    displayMenuThenDirect(Teams)


if __name__ == "__main__":
    main()
