def getLeagueWideData(f_name):
    fin = open(f_name, "r")

    goals_for_at_home = []
    goals_for_away = []

    for line in fin:
        line = line.strip().split("\t")
        if len(line) == 2:
            continue
        goals_for_at_home.append(int(line[2]))
        goals_for_away.append(int(line[3]))

    fin.close()

    def mean(a):
        return round(sum(a) / len(a), 5)

    return mean(goals_for_away), mean(goals_for_at_home)


def main():
    f_name = "PL_201920_Data.txt"
    getLeagueWideData(f_name)


if __name__ == "__main__":
    main()
