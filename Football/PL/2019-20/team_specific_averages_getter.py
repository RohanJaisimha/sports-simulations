def getTeamSpecificData(f_name):
    teams = set()
    fin = open(f_name, "r")
    for i in fin:
        i = i.split("\t")
        teams.add(i[0])
    fin.close()

    teams = sorted(list(teams))
    for team in teams:
        fin = open(f_name, "r")
        goals_for_at_home = []
        goals_for_away = []
        goals_against_at_home = []
        goals_against_away = []
        for line in fin:
            line = line.strip().split("\t")
            if len(line) == 2:
                continue
            if line[0] == team:
                goals_for_at_home.append(int(line[2]))
                goals_against_at_home.append(int(line[3]))
            elif line[1] == team:
                goals_for_away.append(int(line[3]))
                goals_against_away.append(int(line[2]))

        def mean(a):
            return round(sum(a) / len(a), 5)

        yield (
            team,
            mean(goals_for_at_home),
            mean(goals_for_away),
            mean(goals_against_at_home),
            mean(goals_against_away),
            0,
            0,
            len(goals_for_at_home),
            len(goals_for_away),
        )
        fin.close()


def main():
    f_name = "PL_201920_Data.txt"
    getTeamSpecificData(f_name)


if __name__ == "__main__":
    main()
